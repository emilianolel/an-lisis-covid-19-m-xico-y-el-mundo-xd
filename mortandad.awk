#!/usr/bin/awk -f

BEGIN {
	FS=","
	cont1=0
	cont2=0
	cont3=0
	cont4=0
}

{
	if ($13 != "9999-99-99" && NR!=1){
		cont1++;
		frec[$13]++;
		}
	if ($6 != "2" && NR!=0 && $13!="9999-99-99"){
		cont2++;
	}
	else if ($6 != "1" && NR!=0 && $13!="9999-99-99"){
		cont3++;
	}
}

END {
	printf "> El número de muertos en el país hasta día de hoy es: %d\n", cont1;
	printf "> El número de mujeres fallecidas por  covid hasta el día de hoy es: %d\n", cont2;
	printf "> El número de hombres fallecidos por  covid hasta el día de hoy es: %d\n", cont3;
	for(dia in frec){
		printf "El día %s hubo %d defunciones\n", dia, frec[dia] | "sort -k2M -k3n -k3 >> mortandad.dat";
		}
}


