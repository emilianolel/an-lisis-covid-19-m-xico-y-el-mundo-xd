# Análisis Covid-19 México y el Mundo XD

Se analizan los datos actualizados de covid en México y algún otro país que el usuario deseé, de esta manera se puede comparar la evolución de la pandemia en otra región geográfica.

Se usan datos de la página https://serendipia.digital/2020/03/datos-abiertos-sobre-casos-de-coronavirus-covid-19-en-mexico/, de donde se obtienen las gráficas de incremento de casos diarios y el acumulado. La grafica de fallecidos utiliza información de la Secretaría de Salud http://187.191.75.115/gobmx/salud/datos_abiertos/datos_abiertos_covid19.zip y las gráficas de los otros paises se toman de https://opendata.ecdc.europa.eu/covid19/casedistribution/csv.

# MUCHO OJO: la https://serendipia.digital/2020/03/datos-abiertos-sobre-casos-de-coronavirus-covid-19-en-mexico/ dejó de actualizar información el día 17/07/2020 así que hasta ese día funciona el programa.

Pd: el programa que deben correr es el que se llama "analisis.sh", el programa llamado "mortandad.awk" no debe ser ejecutado por el usuario.

Cualquier observación o sugerencia es bienvenida en el siguiente correo: eherrera1331@gmail.com :)
