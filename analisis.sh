#!/bin/bash

#Se llama DIA a la variable que contenga el día de hoy con el formato dd/mm/aaaa
DIA=`date +"%d/%m/%Y"`

#Se le solicita al usuario el intervalo tiempo que quiere analizar y lo almacena en las variables "fecha1" y "fecha2".
echo "Ingresa la fecha, desde la cuál quieres comenzar el análisis, con el siguiente formato ddmmaaaa sin espacios ni guiones o algún otro tipo de separación."
read fecha1
#Las variables definidas abajo almacenan la información del día (d), mes (m) y año (a) de las fechas 1 y 2 respectivamente.
d1=`expr ${fecha1:0:2} + 0`
m1=`expr ${fecha1:2:2} + 0`
a1=`expr ${fecha1:4:4} + 0`

while [[ $a1 -lt 2020 ]] || [[ $a1 -gt `expr ${DIA:6:4} + 0` ]] || [[ $m1 -le 3 ]] && [[ $d1 -le 17 ]] || [[ $d1 -ge `expr ${DIA:0:2} + 0` ]] && [[ $m1 -ge `expr ${DIA:3:2} + 0` ]] 2>/dev/null
do
	echo "Ingresa una fecha desde el 01/04/2020 hasta el día de ayer. Recuerda que debes ingresar  la fecha con el siguiente formato: ddmmaaaa, sin espacios, ni guiones o algún otro tipo de separación."
	read fecha1
	d1=`expr ${fecha1:0:2} + 0`
	m1=`expr ${fecha1:2:2} + 0`
	a1=`expr ${fecha1:4:4} + 0`
done
	
echo "Ingresa la fecha, hasta la cuál quieres concluir el análisis, con el siguiente formato ddmmaaaa sin espacios ni guiones o algún otro tipo de separación."
read fecha2
d2=`expr ${fecha2:0:2} + 0`
m2=`expr ${fecha2:2:2} + 0`
a2=`expr ${fecha2:4:4} + 0`

while [[ $a2 -lt 2020 ]] || [[ $a2 -gt `expr ${DIA:6:4} + 0` ]] || [[ $m2 -le 3 ]] && [[ $d2 -le 17 ]] || [[ $d2 -ge `expr ${DIA:0:2} + 0` ]] && [[ $m2 -ge `expr ${DIA:3:2} + 0` ]] 2>/dev/null
do
	echo "Ingresa una fecha desde el 01/04/2020 hasta el día de ayer. Recuerda que debes ingresar  la fecha con el siguiente formato: ddmmaaaa, sin espacios, ni guiones o algún otro tipo de separación."
	read fecha2
	d2=`expr ${fecha2:0:2} + 0`
	m2=`expr ${fecha2:2:2} + 0`
	a2=`expr ${fecha2:4:4} + 0`
done

clear
echo "Ingresa el número de la entidad de la cual quieres hacer el análisis"
echo "(0) Todo el país.				(17) Colima"
echo "(1) San Luis Potosí				(18) Sinaloa"
echo "(2) Ciudad de México				(19) Durango"
echo "(3) Jalisco					(20) Oaxaca"
echo "(4) Veracruz					(21) Baja California"
echo "(5) Chihuahua					(22) Morelos"
echo "(6) Campeche					(23) Hidalgo"
echo "(7) Guanajuato					(24) Nuevo León"
echo "(8) Tamaulipas					(25) Aguascalientes"
echo "(9) Sonora					(26) Yucatán"
echo "(10) Tlaxcala					(27) México"
echo "(11) Chiapas					(28) Querétaro"
echo "(12) Tabasco					(29) Guerrero"
echo "(13) Quintana Roo				(30) Zacatecas"
echo "(14) Nayarit					(31) Michoacán"
echo "(15) Baja California				(32) Coahuila"
echo "(16) Puebla"
read entidad

if [ $entidad -eq 0 ]
then
	objeto="Los casos totales hasta el día ingresado son"
elif [ $entidad -eq 1 ]
then
	objeto="San Luis Potosí"
elif [ $entidad -eq 2 ]
then
	objeto="Ciudad de México"
elif [ $entidad -eq 3 ]
then
	objeto="Jalisco"
elif [ $entidad -eq 4 ]
then
	objeto="Veracruz"
elif [ $entidad -eq 5 ]
then
	objeto="Chihuahua"
elif [ $entidad -eq 6 ]
then
	objeto="Campeche"
elif [ $entidad -eq 7 ]
then
	objeto="Guanajuato"
elif [ $entidad -eq 8 ]
then
	objeto="Tamaulipas"
elif [ $entidad -eq 9 ]
then
	objeto="Sonora"
elif [ $entidad -eq 10 ]
then
	objeto="Tlaxcala"
elif [ $entidad -eq 11 ]
then
	objeto="Chiapas"
elif [ $entidad -eq 12 ]
then
	objeto="Tabasco"
elif [ $entidad -eq 13 ]
then
	objeto="Quintana Roo"
elif [ $entidad -eq 14 ]
then
	objeto="Baja California"
elif [ $entidad -eq 15 ]
then
	objeto="Nayarit"
elif [ $entidad -eq 16 ]
then
	objeto="Puebla"
elif [ $entidad -eq 17 ]
then
	objeto="Colima"
elif [ $entidad -eq 18 ]
then
	objeto="Sinaloa"
elif [ $entidad -eq 19 ]
then
	objeto="Durango"
elif [ $entidad -eq 20 ]
then
	objeto="Oaxaca"
elif [ $entidad -eq 21 ]
then
	objeto="Baja California Sur"
elif [ $entidad -eq 22 ]
then
	objeto="Morelos"
elif [ $entidad -eq 23 ]
then
	objeto="Hidalgo"
elif [ $entidad -eq 24 ]
then
	objeto="Nuevo León"
elif [ $entidad -eq 25 ]
then
	objeto="Aguascalientes"
elif [ $entidad -eq 26 ]
then
	objeto="México"
elif [ $entidad -eq 27 ]
then
	objeto="Yucatán"
elif [ $entidad -eq 28 ]
then
	objeto="Querétaro"
elif [ $entidad -eq 29 ]
then
	objeto="Guerrero"
elif [ $entidad -eq 30 ]
then
	objeto="Zacatecas"
elif [ $entidad -eq 31 ]
then
	objeto="Coahuila"
elif [ $entidad -eq 32 ]
then
	objeto="Michoacán"
fi

#Posteriormente se crea un directorio llamado DatCov"fecha1"-"fecha2 en el cual se almacenarán los datos a analizar
mkdir DatCov$fecha1-$fecha2

#Estas funciones análisis, extraen la siguiente información de los archivos descargados: total de casos acumulados por día, el total por sexo, y el total por entidad.
analisis0(){
	cero=0
	awk 'BEGIN{FS=","}  END {printf "Los casos totales hasta el día ingresado son: %d\n", NR-1 >> "analisis.txt"}' DatCov$fecha1-$fecha2/$cero$i$cero$j$a1/covid-19-mexico-$cero$i$cero$j$a1.csv 
	sed 's/\"//g' DatCov$fecha1-$fecha2/$cero$i$cero$j$a1/covid-19-mexico-$cero$i$cero$j$a1.csv | awk ' BEGIN{FS=",";sum=0} {if(NR>1){if($3=="F" || $3=="FEMENINO"){sum+=1}}} END{printf "El número de mujeres infectadas es: %d\n", sum >> "analisis.txt"}'
	sed 's/\"//g' DatCov$fecha1-$fecha2/$cero$i$cero$j$a1/covid-19-mexico-$cero$i$cero$j$a1.csv | awk ' BEGIN{FS=",";sum=0} {if(NR>1){if($3=="M" || $3=="MASCULINO"){sum+=1}}} END{printf "El número de hombres infectadas es: %d\n", sum >> "analisis.txt"}'
	sed 's/\"//g' DatCov$fecha1-$fecha2/$cero$i$cero$j$a1/covid-19-mexico-$cero$i$cero$j$a1.csv | awk 'BEGIN{FS=","}{print $2}' | awk 'BEGIN{FS=","}{if(NR>1){for (i=1;i<=NF;i++){frec[$i]++}}}END{for (estado in frec){printf "Los casos confirmados en %s son: %d\n", estado, frec[estado]>> "analisis.txt"}}'
}
#la diferencia entre analisis0 y analisis1 es que, analisis0 agrega un cero a las direcciónes de los directorios ya que se usa para días entre 1 y 9, mientras que en analisis1 no se agregan ceros.
analisis1(){
	cero=0
	awk 'BEGIN{FS=","}  END {printf "Los casos totales hasta el día ingresado son: %d\n", NR-1 >> "analisis.txt"}' DatCov$fecha1-$fecha2/$i$cero$j$a1/covid-19-mexico-$i$cero$j$a1.csv
	sed 's/\"//g' DatCov$fecha1-$fecha2/$i$cero$j$a1/covid-19-mexico-$i$cero$j$a1.csv | awk ' BEGIN{FS=",";sum=0} {if(NR>1){if($3=="F" || $3=="FEMENINO"){sum+=1}}} END{printf "El número de mujeres infectadas es: %d\n", sum >> "analisis.txt"}'
	sed 's/\"//g' DatCov$fecha1-$fecha2/$i$cero$j$a1/covid-19-mexico-$i$cero$j$a1.csv | awk ' BEGIN{FS=",";sum=0} {if(NR>1){if($3=="M" || $3=="MASCULINO"){sum+=1}}} END{printf "El número de hombres infectadas es: %d\n", sum >> "analisis.txt"}'
	sed 's/\"//g' DatCov$fecha1-$fecha2/$i$cero$j$a1/covid-19-mexico-$i$cero$j$a1.csv | awk 'BEGIN{FS=","}{print $2}' | awk 'BEGIN{FS=","}{if(NR>1){for (i=1;i<=NF;i++){frec[$i]++}}}END{for (estado in frec){printf "Los casos confirmados en %s son: %d\n", estado, frec[estado]>> "analisis.txt"}}'
}

#Se define la función descarga bajará los archivos correspondientes al intervalo de tiempo que se señaló anteriormente. Los datos se obtienen de la página https://serendipia.digital/2020/03/datos-abiertos-sobre-casos-de-coronavirus-covid-19-en-mexico/.
descarga(){
	#Variable auxiliar. Más adelante se explica su funcionamiento.
	cero=0
	#Al hacer una suma con cero del valor del mes correspondiente al día 1, la variable j ahora no tiene el formato 02, por ejemplo ahora solo es 2.
	j=`expr $m1 + 0`
	i=`expr $d1 + 0`
	contador=0
	cont=0
	#Se comienza un bucle sobre el valor del mes 1 seleccionado que termina cuando el valor de j coincide con el valor del mes 2.
	while [[ $j -le $m2 ]]
	do
		#Se le añaden condiciones sobre el valor del mes. La primera escrita indica que si el valor de j coincide con el valor del mes 2, entonces:
		if [ $j -eq $m2 ]
		then
			#Aquí checamos los valores iniciales de los meses.
			if [ $m1 -eq $m2 ]
			then
				#Lo mismo que en el caso de los meses.
				i=`expr $d1 + 0`
			else
				#Los meses, usualmente, comienzan en 1
			i=1
			fi
			#se le llama lim al día correspondiente a la fecha 2
			lim=`expr $d2 + 0`
			while [[ $i -le $lim ]]
			do
				#en caso de que el día sea menor que 10 debemos agregar un cero.
				if [ $i -lt 10 ] 
				then
					mkdir DatCov$fecha1-$fecha2/$cero$i$cero$j$a1
					if [ $j -le 4 ]
					then
						wget -P DatCov$fecha1-$fecha2/$cero$i$cero$j$a1/ https://serendipia.digital/wp-content/uploads/$a1/$cero$j/Tabla_casos_positivos_COVID-19_resultado_InDRE_$a1.$cero$j.$cero$i-Table-1.csv
						mv DatCov$fecha1-$fecha2/$cero$i$cero$j$a1/*.csv DatCov$fecha1-$fecha2/$cero$i$cero$j$a1/covid-19-mexico-$cero$i$cero$j$a1.csv
						analisis0
						cont=`expr $cont + 1`
					else
						wget -P DatCov$fecha1-$fecha2/$cero$i$cero$j$a1/ https://serendipia.digital/wp-content/uploads/$a1/$cero$j/covid-19-mexico-$cero$i$cero$j$a1.csv
						analisis0
						cont=`expr $cont + 1`
					fi
				#en esta condición se considera 
				elif [ $i -le 18 ] && [ $j -le 4 ]
				then
					mkdir DatCov$fecha1-$fecha2/$i$cero$j$a1
					wget -P DatCov$fecha1-$fecha2/$i$cero$j$a1/ https://serendipia.digital/wp-content/uploads/$a1/$cero$j/Tabla_casos_positivos_COVID-19_resultado_InDRE_$a1.$cero$j.$i-Table-1.csv
					mv DatCov$fecha1-$fecha2/$i$cero$j$a1/*.csv DatCov$fecha1-$fecha2/$i$cero$j$a1/covid-19-mexico-$i$cero$j$a1.csv
					analisis1
					cont=`expr $cont + 1`
				elif [ $i -eq 21 ] && [ $j -eq 04 ]
				then
					mkdir DatCov$fecha1-$fecha2/2104$a1
					wget -P DatCov$fecha1-$fecha2/2104$a1/ https://serendipia.digital/wp-content/uploads/2020/04/covid-19-mexico-21042020-2.csv
					cont=`expr $cont + 1`
				else
					mkdir DatCov$fecha1-$fecha2/$i$cero$j$a1
					wget -P DatCov$fecha1-$fecha2/$i$cero$j$a1/ https://serendipia.digital/wp-content/uploads/$a1/$cero$j/covid-19-mexico-$i$cero$j$a1.csv
					analisis1
					cont=`expr $cont + 1`
				fi
				sleep .5
				i=`expr $i + 1`
			done
		elif [ $j -lt $m2 ]
		then
			if [ $contador -eq 0 ]
			then
                i=`expr $d1 + 0`
		    else
                i=1
            fi
			if [ $(($j % 2)) -ne 0 ]
			then
				lim=31
			else
				lim=30
			fi
			contador=`expr $contador + 1`
			while [[ $i -le $lim ]]
			do
      	#mkdir DatCov$fecha1-$fecha2/$cero$i$cero$j$a1
		 	if [ $i -lt 10 ]
		    then
       	    	mkdir DatCov$fecha1-$fecha2/$cero$i$cero$j$a1
	   	 		if [ $j -le 4 ]
       	        then
					wget -P DatCov$fecha1-$fecha2/$cero$i$cero$j$a1/ https://serendipia.digital/wp-content/uploads/$a1/$cero$j/Tabla_casos_positivos_COVID-19_resultado_InDRE_$a1.$cero$j.$cero$i-Table-1.csv
					mv DatCov$fecha1-$fecha2/$cero$i$cero$j$a1/*.csv DatCov$fecha1-$fecha2/$cero$i$cero$j$a1/covid-19-mexico-$cero$i$cero$j$a1.csv
	   	 			analisis0
	   	 			cont=`expr $cont + 1`
	   	 		else
					wget -P DatCov$fecha1-$fecha2/$cero$i$cero$j$a1/ https://serendipia.digital/wp-content/uploads/$a1/$cero$j/covid-19-mexico-$cero$i$cero$j$a1.csv
	   	 			analisis0
	   	 			cont=`expr $cont + 1`
				fi
			elif [ $i -le 18 ] && [ $j -le 4 ]
			then
				mkdir DatCov$fecha1-$fecha2/$i$cero$j$a1
				wget -P DatCov$fecha1-$fecha2/$i$cero$j$a1/ https://serendipia.digital/wp-content/uploads/$a1/$cero$j/Tabla_casos_positivos_COVID-19_resultado_InDRE_$a1.$cero$j.$i-Table-1.csv
				mv DatCov$fecha1-$fecha2/$i$cero$j$a1/*.csv DatCov$fecha1-$fecha2/$i$cero$j$a1/covid-19-mexico-$i$cero$j$a1.csv
	   	 		analisis1
	   	 		cont=`expr $cont + 1`
	   	 	elif [ $i -eq 21 ] && [ $j -eq 04 ]
	   	 	then
				mkdir DatCov$fecha1-$fecha2/2104$a1
	   	 		wget -P DatCov$fecha1-$fecha2/2104$a1/ https://serendipia.digital/wp-content/uploads/2020/04/covid-19-mexico-21042020-2.csv
	   	 		cont=`expr $cont + 1`
	   	 	else
				mkdir DatCov$fecha1-$fecha2/$i$cero$j$a1
				wget -P DatCov$fecha1-$fecha2/$i$cero$j$a1/ https://serendipia.digital/wp-content/uploads/$a1/$cero$j/covid-19-mexico-$i$cero$j$a1.csv
	   	 		analisis1
	   	 		cont=`expr $cont + 1`
			fi
			sleep .5
			i=`expr $i + 1`
			done
		fi
		j=`expr $j + 1`
	done
	wget -P DatCov$fecha1-$fecha2/ http://187.191.75.115/gobmx/salud/datos_abiertos/datos_abiertos_covid19.zip
	unzip DatCov$fecha1-$fecha2/*.zip
	mv *.csv DatCov$fecha1-$fecha2/
	rm DatCov$fecha1-$fecha2/*.zip
	mv DatCov$fecha1-$fecha2/*.csv DatCov$fecha1-$fecha2/DAT.csv
	sed 's/"//g' "DatCov$fecha1-$fecha2/DAT.csv" >> DatCov$fecha1-$fecha2/DATOSOFICIALES.csv
	rm DatCov$fecha1-$fecha2/DAT.csv
	#Se descarga información de casos covid en el mundo de https://data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data
	#Nos va a crear un archivo llamado "csv".
	wget -P DatCov$fecha1-$fecha2/ https://opendata.ecdc.europa.eu/covid19/casedistribution/csv
}

#Se llama a la función descarga que, además de descargar los datos, hace uso de la funciones análisis para extraer la información de los archivos descargados.
descarga

clear

mv analisis.txt DatCov$fecha1-$fecha2/

#awk 'BEGIN{FS=":"} /Los casos totales hasta el día ingresado son/ {print $2}' DatCov$fecha1-$fecha2/analisis.txt > DatCov$fecha1-$fecha2/aux.dat
awk -v name="$objeto" 'BEGIN{FS=":"} $0 ~ name {print $2}' DatCov$fecha1-$fecha2/analisis.txt > DatCov$fecha1-$fecha2/aux.dat

awk 'BEGIN{FS=","}{if (NR!=1 && $8!="JPG11668"){frec[$7]++}}END{for(pais in frec){printf "%s\n",pais}}' DatCov$fecha1-$fecha2/csv | sort -k7 >> paises.txt
a=`wc -l paises.txt`
num=`expr ${a:0:3} + 0`
for i in `seq 1 $num`; do echo $i>>numeros.dat; done
paste numeros.dat paises.txt >> listpaises.dat
rm numeros.dat
cat listpaises.dat
echo "Ingresa el número del país con el que quieres hacer la comparación."
read pais
arr=(`cat paises.txt`)
p=${arr[`expr $pais - 1`]}
awk -v pais="$p" 'BEGIN{FS=","}{if($7==pais){print $0}}' DatCov$fecha1-$fecha2/csv >>DatCov$fecha1-$fecha2/country.dat
rm paises.txt listpaises.dat
sort -t"," -k2M -k3n -k1 DatCov$fecha1-$fecha2/country.dat >> DatCov$fecha1-$fecha2/COUNTRY.DAT
rm DatCov$fecha1-$fecha2/country.dat

clear
#script de bash que obtiene la mortandad de los datos oficiales.
./mortandad.awk DatCov$fecha1-$fecha2/*.csv
mv mortandad.dat DatCov$fecha1-$fecha2/

arr=(`cat DatCov$fecha1-$fecha2/aux.dat`)
k=1
contador=`expr ${#arr[@]} - 2`
for i in `seq 0 $contador`
do 
	echo "${arr[$k]}-${arr[$i]}" | bc -l >> DatCov$fecha1-$fecha2/aux1.dat
	k=`expr $k + 1`
done

for i in `seq 1 $cont`
do
	echo $i >> DatCov$fecha1-$fecha2/aux3.dat
done

paste DatCov$fecha1-$fecha2/aux3.dat DatCov$fecha1-$fecha2/aux1.dat >> DatCov$fecha1-$fecha2/info.dat

#arr1=(`cat DatCov$fecha1-$fecha2/aux1.dat`)
#l=0
#m=0
#len=`expr $cont - 1`
#while [[ $l -lt $len ]]
#do
#	param=${arr1[$l]}
#	if [ "$param" -gt "$m" ] 2>/dev/null 
#	then
#		m=$param
#	fi
#	l=`expr $l + 1`
#done
#alfa=`expr $m + 1 / 4 \* $m`
#
#arr1=(`cat DatCov$fecha1-$fecha2/aux.dat`)
#l=0
#m=0
#len=`expr $cont - 1`
#while [[ $l -lt $len ]]
#do
#	param=${arr1[$l]}
#	if [ "$param" -gt "$m" ] 2>/dev/null 
#	then
#		m=$param
#	fi
#	l=`expr $l + 1`
#done
#alfa1=`expr $m + 1 / 4 \* $m`

graficarcd(){
echo "set title '-- Incremento de casos diarios en $objeto desde el $d1/$m1/$a1 hasta el $d2/$m2/$a2 --'" >DatCov$fecha1-$fecha2/graf.gnu
echo "set xlabel 'DIAS'">>DatCov$fecha1-$fecha2/graf.gnu
echo "set ylabel 'NUEVOS CASOS'">>DatCov$fecha1-$fecha2/graf.gnu
echo "set boxwidth 0.5">>DatCov$fecha1-$fecha2/graf.gnu
echo "set style fill solid">>DatCov$fecha1-$fecha2/graf.gnu
echo "set autoscale">>DatCov$fecha1-$fecha2/graf.gnu
#echo "set xrange [0:$((cont+1))]">>DatCov$fecha1-$fecha2/graf.gnu
#echo "set yrange [0:$alfa]">>DatCov$fecha1-$fecha2/graf.gnu
echo "plot 'DatCov$fecha1-$fecha2/info.dat' with boxes">>DatCov$fecha1-$fecha2/graf.gnu
cat DatCov$fecha1-$fecha2/graf.gnu | gnuplot -p
}
graficarcd

graficarca(){
paste DatCov$fecha1-$fecha2/aux3.dat DatCov$fecha1-$fecha2/aux.dat >> DatCov$fecha1-$fecha2/info1.dat
echo "set title '-- Casos acumulados en $objeto desde el $d1/$m1/$a1 hasta el $d2/$m2/$a2 --'" >DatCov$fecha1-$fecha2/graf1.gnu
echo "set xlabel 'DIAS'">>DatCov$fecha1-$fecha2/graf1.gnu
echo "set ylabel 'CASOS ACUMULADOS'">>DatCov$fecha1-$fecha2/graf1.gnu
echo "set boxwidth 0.5">>DatCov$fecha1-$fecha2/graf1.gnu
echo "set style fill solid">>DatCov$fecha1-$fecha2/graf1.gnu
echo "set autoscale">>DatCov$fecha1-$fecha2/graf.gnu
#echo "set xrange [0:$((cont+1))]">>DatCov$fecha1-$fecha2/graf1.gnu
#echo "set yrange [0:$alfa1]">>DatCov$fecha1-$fecha2/graf1.gnu
echo "plot 'DatCov$fecha1-$fecha2/info1.dat' with boxes">>DatCov$fecha1-$fecha2/graf1.gnu
cat DatCov$fecha1-$fecha2/graf1.gnu | gnuplot -p
}
graficarca

graficarm(){
	echo "set title '-- Muertos diarios en el país hasta el día de hoy --'" >DatCov$fecha1-$fecha2/mortandad.gnu
	echo "set xlabel 'DIAS'">>DatCov$fecha1-$fecha2/mortandad.gnu
	echo "set ylabel 'MUERTES DIARIAS'">>DatCov$fecha1-$fecha2/mortandad.gnu
	echo "set boxwidth 0.5">>DatCov$fecha1-$fecha2/mortandad.gnu
	echo "set style fill solid">>DatCov$fecha1-$fecha2/mortandad.gnu
	echo "set autoscale">>DatCov$fecha1-$fecha2/mortandad.gnu
	echo "plot 'DatCov$fecha1-$fecha2/mortandad.dat' using 5 with boxes">> DatCov$fecha1-$fecha2/mortandad.gnu
	cat DatCov$fecha1-$fecha2/mortandad.gnu | gnuplot -p
}
graficarm

grafcom(){
	echo "set title '-- Muertos diarios en $p hasta el día de hoy --'" >DatCov$fecha1-$fecha2/mortandad_$p.gnu
	echo "set xlabel 'DIAS'">>DatCov$fecha1-$fecha2/mortandad_$p.gnu
	echo "set ylabel 'MUERTES DIARIAS'">>DatCov$fecha1-$fecha2/mortandad_$p.gnu
	echo "set boxwidth 0.5">>DatCov$fecha1-$fecha2/mortandad_$p.gnu
	echo "set style fill solid">>DatCov$fecha1-$fecha2/mortandad_$p.gnu
	echo "set autoscale">>DatCov$fecha1-$fecha2/mortandad_$p.gnu
	echo "set datafile separator comma">>DatCov$fecha1-$fecha2/mortandad_$p.gnu
	echo "plot 'DatCov$fecha1-$fecha2/COUNTRY.DAT' using 6 with boxes">> DatCov$fecha1-$fecha2/mortandad_$p.gnu
	cat DatCov$fecha1-$fecha2/mortandad_$p.gnu | gnuplot -p
}
grafcom

grafdia(){
	echo "set title '-- nuevos casos diarios en $p hasta el día de hoy --'" >DatCov$fecha1-$fecha2/dia_$p.gnu
	echo "set xlabel 'DIAS'">>DatCov$fecha1-$fecha2/dia_$p.gnu
	echo "set ylabel 'NUEVOS CASOS DIARIAS'">>DatCov$fecha1-$fecha2/dia_$p.gnu
	echo "set boxwidth 0.5">>DatCov$fecha1-$fecha2/dia_$p.gnu
	echo "set style fill solid">>DatCov$fecha1-$fecha2/dia_$p.gnu
	echo "set autoscale">>DatCov$fecha1-$fecha2/dia_$p.gnu
	echo "set datafile separator comma">>DatCov$fecha1-$fecha2/dia_$p.gnu
	echo "plot 'DatCov$fecha1-$fecha2/COUNTRY.DAT' using 5 with boxes">> DatCov$fecha1-$fecha2/dia_$p.gnu
	cat DatCov$fecha1-$fecha2/dia_$p.gnu | gnuplot -p
}
grafdia

